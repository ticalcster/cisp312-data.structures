import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Vector;

public class counter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Vector<CharFreq> chfq = new Vector<CharFreq>();
		
		// initialize the array
		for(int i=0; i <= 255; i++) {
			chfq.add(new CharFreq() );
		}
		
		try{
		    PrintWriter outFile = new PrintWriter(args[1]);
			/*
			 * open file with a buffer to allow for 
			 * large files like war and peace :)
			 */
			FileInputStream fstream = new FileInputStream(args[0]);
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			
			char[] cbuf = new char[1];
			while ( (br.read(cbuf)) != -1 ) {
				chfq.get( (int)cbuf[0] ).inc();
			}
			//Close the input stream
			in.close();
			
			for(int i=0; i< chfq.size(); i++) {
				if(chfq.get(i).count() > 0) {
					System.out.format("%s, %d times\n",(char)i,chfq.get(i).count() );
					outFile.format("%s, %d times",(char)i,chfq.get(i).count() );
				}
			}
			
			//Close the output stream
			outFile.close();
			
		}catch (Exception e){//Catch exception if the any
			System.err.println("Error: " + e.getMessage());
		}
	}

}

class CharFreq {
	private int count = 0;
	public CharFreq(){

	}
	
	public void inc() {
		 this.count++;
	}
	
	public int count() {
		return this.count;
	}

}
