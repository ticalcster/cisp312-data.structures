package huffman.util.tree;

import java.util.List;
import java.util.Vector;

public class Node { // implements Comparable<Node>{  //TODO: may not need Comparable
	//TODO: change to private (all)?
	/* char */
	protected boolean leaf;
	protected byte ch; //TODO: rename
	protected int count; //also known as weight
	
	/* tree */
	//TODO: change from string
	//private String charlist = "";
	protected List<Byte> bytelist = new Vector<Byte>();
	protected Node node[];// = new Node[2];
	
	/* list */
	private Node nextNode;
	private int	length = 1;

	/**
	 * Create a new leaf node.
	 * @param ch is the Character the leaf will hold.
	 */
	public Node(byte ch) {
		this.ch = ch;
		this.count = 0;
		this.bytelist.add(ch);
		this.leaf = true;
	}
	/**
	 * Create a new branch node.
	 * @param zero node to place in array[0].
	 * @param zero node to place in array[1].
	 */
	public Node(Node zero, Node one) {
		this.node = new Node[2];
		this.node[0] = zero;
		this.node[1] = one;
		this.count = zero.weight() + one.weight();
		this.leaf = false;
		this.bytelist.addAll(zero.bytelist);
		this.bytelist.addAll(one.bytelist);
	}
	/**
	 * This will build a tree from a Node list.
	 * @param tree is the node list to build a tree from.
	 */
	public static Node buildATree(Node tree) {
		while(tree.listLength() > 1) {
			tree = tree.buildTree();
		}
		return tree;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isLeaf() {
		return this.leaf;
	}
	/**
	 * Increments the char count by one.
	 */
	public void inc() {
		//TODO: thow exception if !leaf
		if( this.leaf ) {
			this.count++;
		}
	}
	/**
	 * 
	 * @return The Node weight, also know as the char count.
	 */
	public int weight() {
		return this.count;
	}
	/**
	 * 
	 * @return The Nodes character.
	 */
	public short getChar() {
		return this.ch;
	}
	/**
	 * 
	 * @return The Characters in this Node and any sub-Nodes in the tree.
	 */
	public List<Byte> getByteList() {
		return this.bytelist;
	}
	
	//TODO: remove (maybe? i can't remember why) 
	/**
	 * 
	 * @return The next Node in the Node list.
	 */
	public Node getNext() {
		return this.nextNode;
	}
	/**
	 * 
	 * @return The length of the node list at current Nodes position.
	 */
	public int listLength() {
		return this.length;
	}
	/**
	 * 
	 * @param chatlist The int List to locate char.
	 * @return The character at given location.
	 * @throws NodeException
	 */
	public short getChar(List<Integer> chatlist) throws NodeException {
		return this.getChar(chatlist,-1);
	}
	/**
	 * 
	 * @param charlist The int List to locate char.
	 * @param index The level the current node is at. Used as the List index.
	 * @return The character at given location. 
	 * @throws NodeException
	 */
	private short getChar(List<Integer> charlist, int index) throws NodeException {
		index++;
		if( this.leaf && (index == charlist.size()) ) {
			if( this.leaf ) {
				return this.ch;
			} else {
				throw new NodeException("Node is null first");
			}
		} else {
			if(this.node != null) {
				return this.node[charlist.get(index)].getChar(charlist, index);
			} else {
				throw new NodeException("Node is null");
			}
		}
	}
	/**
	 * 
	 * @param c The character to create the bit List.
	 * @return A List of bits to get to the given character.
	 */
	public List<Integer> getBitArray(byte b) {
		//TODO: first check if char is in tree
		return this.getBitArray(b, new Vector<Integer>() );
	}
	/**
	 * 
	 * @param c The character to create the bit List.
	 * @param bits The bit List.
	 * @return A List of bits to get to the given character.
	 */
	private List<Integer> getBitArray(byte c, List<Integer> bits) {
		if( !this.leaf ) {
			if( node[0].getByteList().contains(c) ) {
				bits.add(0);
				node[0].getBitArray(c, bits);
			} else if( node[1].getByteList().contains(c) ) {
				bits.add(1);
				node[1].getBitArray(c, bits);
			} else {
				//TODO: do something, maybe through exception
			}
		}
		return bits;
	}
	/**
	 * This function is used to help create a tree structure from the node list.
	 * Running the function once will take the two lowest weighted Nodes and combine
	 * them. The new branch Node is then sent back into the node list to be sorted.
	 * 
	 * @return Node on step closer to a Tree.
	 */
	public Node buildTree() {
		if( this.length >= 3) {
			//TODO: maybe should 0 out the old lengths used by the node list.
			//TODO: maybe should remove nextNode reference used by the node list.
			return new Node(this, this.nextNode).addSorted(this.nextNode.nextNode);
		} else if( this.length == 2 ) {
			return new Node(this, this.nextNode);
		} else {
			return this;
		}
	}
	/**
	 * Used to create a node list.
	 * When adding a Node to the node list it will be inserted
	 * in the correctly sorted location.  After all Nodes are 
	 * added to the node list then a tree can be created.
	 * 
	 * @param n The Node to add to the array.
	 * @return Node on step closer to a Tree.
	 */
	public Node addSorted(Node n) {
		//TODO: check if char node n is already in list
		if( this.weight() > n.weight() ) {
		//if( this.compareTo(n) > 0 ) {
			n.addSorted(this);
			return n;
		} else {
			if( this.nextNode != null) {
				this.nextNode = this.nextNode.addSorted(n);
			} else {
				this.nextNode = n;
				this.length = n.length;
			}
			this.length++;
			return this;
		}
	}
	/**
	 * 
	 * @param n The Node to compare too.
	 * @return The integer value of the difference.
	 */
	public int compareTo(Node n) {
		int firstCompare = this.weight() - n.weight(); //first compare weights.
		return firstCompare != 0 ? firstCompare : this.ch - n.ch;  // if weights are diff compare the byte being stored.
    }
	
	public void save(List<Byte> blist, List<Integer> ilist) {
		blist.clear();
		ilist.clear();
		this.save(blist, ilist, true);
	}
	public void save(List<Byte> blist, List<Integer> ilist, boolean go) {
		if( this.leaf ) {
			blist.add(this.ch);
			ilist.add(this.count);
		} else {
			this.node[0].save(blist, ilist,true);
			this.node[1].save(blist, ilist,true);
		}
	}
	
	/* debugish stuff */
	/**
	 * Finds the efficacy of the encoding table.
	 * @return The ratio of characters per bit.
	 */
	public double optimal() {
		return (double)this.optimal(-1) / (double)this.weight();
	}
	private int optimal(int level) {
		level++;
		int bits = 0;
		if( !this.leaf ) {
			bits += node[0].optimal(level);
			bits += node[1].optimal(level);
		} else {
			//System.out.println(this.toString() + " bits:" + level );
			bits = this.weight() * level;
		}
		return bits;
	}
	
	public void print() {
		this.printList(0);
	}
	private void printList(int index) {
		if( this.leaf ) {
			//System.out.println(index + " " + this.toString() + " Length:" + this.length );
			this.printTree();
		} else {
			this.printTree();
		}
		if( this.nextNode != null) {
			this.nextNode.printList(++index);
		}
	}
	
    public void printTree() {
        this.printTree("", true);
    }
    private void printTree(String prefix, boolean isTail) {
    	//System.out.println(prefix + (isTail ? "+-- " : "+-- ") + this.toString() );
    	System.out.println(prefix + (isTail ? "└── " : "├── ") + this.toString() );
    	if( !leaf ) {
    		//this.node[0].printTree(prefix + (isTail ? "    " : "|   "), false);
    		//this.node[1].printTree(prefix + (isTail ? "    " : "|   "), true);
    		this.node[0].printTree(prefix + (isTail ? "    " : "│   "), false);
    		this.node[1].printTree(prefix + (isTail ? "    " : "│   "), true);
    	}
    }

	@Override
	public String toString() {
		String str = "";
		if(this.leaf) {
			str += this.humanReadable(this.ch);
		}
		return str + ":" + this.weight() + " ";// + this.getCharList();
	}
	
	private String humanReadable(short ch) {
		String str = "";
		if(this.leaf) {
			switch(this.ch) {
				case 0: str += "(Null char)";break;
				case 1: str += "(Start of Heading)";break;
				case 2: str += "(Start of Text)";break;
				case 3: str += "(End of Text)";break;
				case 4: str += "(End of Transmission)";break;
				case 5: str += "(Enquiry)";break;
				case 6: str += "(Acknowledgment)";break;
				case 7: str += "(Bell)";break;
				case 8: str += "(Back Space)";break;
				case 9: str += "(Horizontal Tab)";break;
				case 10: str += "(Line Feed)";break;
				case 11: str += "(Vertical Tab)";break;
				case 12: str += "(Form Feed)";break;
				case 13: str += "(Carriage Return)";break;
				case 14: str += "(Shift Out / X-On)";break;
				case 15: str += "(Shift In / X-Off)";break;
				case 16: str += "(Data Line Escape)";break;
				case 17: str += "(Device Control 1)";break;
				case 18: str += "(Device Control 2)";break;
				case 19: str += "(Device Control 3)";break;
				case 20: str += "(Device Control 4)";break;
				case 21: str += "(Negative Acknowledgement)";break;
				case 22: str += "(Synchronous Idle)";break;
				case 23: str += "(End of Transmit Block)";break;
				case 24: str += "(Cancel)";break;
				case 25: str += "(End of Medium)";break;
				case 26: str += "(Substitute)";break;
				case 27: str += "(Escape)";break;
				case 28: str += "(File Separator)";break;
				case 29: str += "(Group Separator)";break;
				case 30: str += "(Record Separator)";break;
				case 31: str += "(Unit Separator)";break;
				case 32: str += "(Space)";break;
				default: str += this.ch;
			}
		}
		return str;
	}
}