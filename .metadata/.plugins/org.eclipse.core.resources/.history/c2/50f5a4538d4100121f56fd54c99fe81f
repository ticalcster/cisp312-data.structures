package huffman.util;

import huffman.io.BitOutputStream;
import huffman.util.tree.Node;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

public class Compress {
	private	InputStream INPUT_STREAM;
	private	BitOutputStream OUTPUT_STREAM;
	private int READLIMIT = 1073741824; //TODO: is this the best way? idk
	
	// only global to allow printing of tree.
	private Node tree = null;
	
	/**
	 * 
	 * @param inStream
	 * @param outStream
	 * @throws IOException 
	 */
	public Compress(BufferedInputStream inStream, BitOutputStream outStream) throws IOException {
		this.INPUT_STREAM = inStream;
		this.OUTPUT_STREAM = outStream;
		this.INPUT_STREAM.mark(READLIMIT); // to reset the file, we need to read it twice.
		this.run();
	}
	
	public Compress(BufferedInputStream inStream, OutputStream outStream) throws IOException {
		this( inStream, new BitOutputStream( outStream ) );
	}
	
	public Compress(BufferedInputStream inStream) throws IOException {
		this( inStream, System.out );
	}
	/**
	 * 
	 * @param infile
	 * @param outfile
	 * @throws IOException 
	 */
	public Compress(String infile, String outfile) throws IOException {
		this( new BufferedInputStream( new FileInputStream( infile )), 
				new BitOutputStream( outfile ) );
	}
	/**
	 * 
	 * @param infile
	 * @throws IOException 
	 */
	public Compress(String infile) throws IOException {
		this( new BufferedInputStream( new FileInputStream( infile )));
	}

	
	private void run() throws IOException {
		/*
		 * count chars
		 */
		List<Node> chfq = countBytes();
		
		/*
		 * Sort / Add to node list.
		 */
		tree = sortBytes(chfq);
		
		/*
		 * create tree
		 */
		tree = Node.buildATree(tree); //TODO: remove the return
		
		/*
		 * output binary data to outfile
		 */
		fileToStream();
	}
	
	
	private List<Node> countBytes() throws IOException {
		List<Node> chfq = new Vector<Node>();
		
		// initialize the array
		for(int i=0; i <= 255; i++) {
			chfq.add(new Node( (byte)i) );
		}
	    
		byte[] buffer = new byte[1];
		while ( (INPUT_STREAM.read(buffer)) != -1 ) {
			chfq.get( unsignedToBytes(buffer[0]) ).inc();
		}
		
		return chfq;
	}
	private Node sortBytes(List<Node> chfq) {
		Node tree = null;
		boolean firstNode = true;
		
		for(int i=0; i< chfq.size(); i++) {
			if(chfq.get(i).weight() > 0) {
				//TODO: remove second if
				if( firstNode ) {
					tree = chfq.get(i);
					firstNode = false;
				} else {
					tree = tree.addSorted( chfq.get(i) );
				}
			}
		}
		
		return tree;
	}
	private void headToStream() throws IOException {
		OUTPUT_STREAM.writeByte((byte)'K');
		OUTPUT_STREAM.writeByte((byte)'C');
	}
	private void fileToStream() throws IOException {
		INPUT_STREAM.reset();

		byte[] buffer = new byte[1];
		while ( (INPUT_STREAM.read(buffer)) != -1 ) {
			OUTPUT_STREAM.writeBit( tree.getBitArray( buffer[0] ));
		}
	}
	
	
	
	public void printTree() {
		tree.print();
	}
	private int unsignedToBytes(byte b) {
		return b & 0xFF;
	}
	
}
