
public class CronItem {
	private String name;
	private int timeneeded;
	private long runtime;
	
	CronItem(String name, int timeneeded, long runtime) {
		this.timeneeded = timeneeded;
		this.runtime = runtime;
	}
	
	public long getRunTime() {
		return this.runtime;
	}
	public long getTimeNeeded() {
		return this.timeneeded;
	}
}
