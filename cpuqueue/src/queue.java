import java.util.LinkedList;
import java.util.Queue;


public class queue {

	/* 
	 * The scheduler is here to avoid doing extra work to let
	 * the cron process access it.
	 */
	public static Scheduler schedule = new Scheduler(); // create the scheduler
	/**
	 * @param args
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		
		CPU cpu = new CPU();  // init cpu for doing work
		
		
		
		//Cron cron = new Cron('#', 1);
		//Cron.addTask("asdf", 10, 20);
		//Cron.addTask("qwer", 10, 40);
		//Cron.addTask("zxcv", 10, 45);
		
		
		schedule.offer(new Process( CPU.nextPID(), 30 ));
		//Scheduler.addToQ(new Process( CPU.nextPID(), 2 ));
		//schedule.add(new Process( CPU.nextPID(), 2 ));
		//s.add(new Process( CPU.nextPID(), 5 ));
		
		while( !schedule.isEmpty() ) {
			schedule.offer( cpu.run( schedule.poll() ));
		}
		
		System.out.println("CPU ended work at: " + CPU.TIME);

		
		/*
		// TODO Auto-generated method stub
		int qtime[] = { 0, 1, 2, 3, 5, 8 };
		//Queue<Process> q = new LinkedList();
		Queue<Process> q[] = new Queue[6];
		
		for(int i = 0; i < 6; i++) {
			q[i] = new LinkedList<Process>();
		}
		
		Process p = new Process('a');
		
		q[0].add( p );
		q[0].add( new Process('b') );
		q[0].add( new Cron('#'));
		
		System.out.println(q[0].element());
		q[0].poll().doWork(4);
		System.out.println(q[0].element());
		q[0].poll().doWork(4);
		System.out.println(q[0].element());
		q[0].poll().doWork(4);
		
		//System.out.println(q[0].element() );
		
		print(qtime);
		*/
	}
	
	/*
	private static void print(int qtime[]) throws InterruptedException {
		String timelength = "4";
		String queuelength = "6";
		
		System.out.format(" q   %"+timelength+"s | %n","time");
		Thread.sleep(CLOCK_SPEED);
		System.out.format(" 0 | %"+timelength+"d | %n",qtime[0]);
		Thread.sleep(CLOCK_SPEED);
		System.out.format(" 1 | %"+timelength+"d | %n",qtime[1]);
		Thread.sleep(CLOCK_SPEED);
		System.out.format(" 2 | %"+timelength+"d | %n",qtime[2]);
		Thread.sleep(CLOCK_SPEED);
		System.out.format(" 3 | %"+timelength+"d | %n",qtime[3]);
		Thread.sleep(CLOCK_SPEED);
		System.out.format(" 4 | %"+timelength+"d | %n",qtime[4]);
		Thread.sleep(CLOCK_SPEED);
		System.out.format(" 5 | %"+timelength+"d | %n",qtime[5]);

	}
	*/
	

}
