
public class Process {
	char pid; // process identifier
	int timegiven = 0; //total time given so far
	int timedoing = 0; //time given from from the queue
	int timeneeded; //process estimated time needed
	
	Process(char pid, int time) {
		this.pid = pid;
		this.setTimeNeeded(time);
	}
	
	/*
	 * this is so the cron process can re-estimated its own
	 * time need to run through the crontab.
	 */
	private void setTimeNeeded(int time) {
		this.timeneeded = time;
	}
	
	public char getPID() {
		return this.pid;
	}
	
	public void doWork() {
		
	}
	
	public final boolean canWork() {
		if( this.timedoing > 0 && this.timeneeded > 0) {
			this.timeneeded--;
			this.timedoing--;
			this.timegiven++;
			System.out.println("Process|"+this.pid+"|: times "+this.timeneeded+" "+this.timedoing+" "+this.timegiven+"");
			return true;
		} else {
			return false;
		}
	}
}
