
public class CPU {
	static int CLOCK_SPEED = 100; // milliseconds to sleep in between CPU cycles
	static long TIME = 0; //TODO: make getter, setter?
	static char PID = 'a';
	
	static char nextPID() {
		return CPU.PID++;
	}
	
	CPU() {
		
	}

	public Process run(Process p) {
		while( p.canWork() ) {
			CPU.TIME++;
			try {
				Thread.sleep(CPU.CLOCK_SPEED);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			p.doWork();
		}
		return p;
	}
	
}
