import java.util.LinkedList;
import java.util.Queue;

public class Scheduler {
	private static Queue<Process> Q = new LinkedList<Process>();
	private int qtime[]; //array for time to spend in each queue
	private int qcheck[]; //array of total time before moving to next queue
	private Queue<Process> q[]; //array of queues
	private int qcount; //number to queue to use
	private boolean processQ = true; //TODO: maybe not needed
	// by default every Scheduler class checks the global in queue.
	
	Scheduler() {
		// create the default timing for the scheduler
		this.qtime = new int[5];
		this.qtime[0] = 1;
		this.qtime[1] = 2;
		this.qtime[2] = 3;
		this.qtime[3] = 5;
		this.qtime[4] = 8;
		this.qcount = this.qtime.length;
		init();
	}
	Scheduler(int qtime[]) {
		this.qtime = qtime;
		this.qcount = this.qtime.length;
		init();
	}
	
	private void init() {
		this.q = new Queue[this.qcount];
		this.qcheck = new int[this.qcount];
		
		for(int i = 0; i < this.qcount; i++) {
			q[i] = new LinkedList<Process>();
		}
		
		for(int i = 0; i < this.qcount; i++) {
			int value = 0;
			for(int j = 0; j <= i; j++) {
				value += qtime[j];
			}
			this.qcheck[i] = value;
			System.out.println("i:"+i+" qtime:"+qtime[i]+" - "+value);
		}
	}
	
	public boolean processQ() {
		return this.processQ;
	}
	public void processQ(boolean bool) {
		this.processQ = bool;
	}
	
	public static boolean addToQ(Process p) {
		Q.add(p);
		return true;
	}
	private void checkQ() {
		if( this.processQ && !Q.isEmpty()) {
			this.offer( Q.poll() );
		}
	}
	
	public boolean offer(Process p) {
		this.checkQ();
		for(int i = 0; i < this.qcount; i++ ) {
			if( (p.timegiven < this.qcheck[i] || ( p.timegiven >= this.qcheck[qcount-1] && i == qcount-1 )) && p.timeneeded > 0 ) {
				System.out.println("added "+p.getPID()+" to q:"+i);
				p.timedoing = qtime[i];
				q[i].add(p);
				break;
			}
		}	
		return true;
	}
	public Process poll() {
		this.checkQ();
		for(int i = 0; i < this.qcount; i++) {
			if(!q[i].isEmpty()) {
				System.out.println("poll "+q[i].peek().getPID()+" from q:"+i);
				return q[i].poll();
			}
		}
		return null;
	}
	public boolean isEmpty() {
		this.checkQ();
		for(int i = 0; i < this.qcount; i++) {
			if(!q[i].isEmpty()) {
				return false;
			}
		}
		return true;
	}
}
