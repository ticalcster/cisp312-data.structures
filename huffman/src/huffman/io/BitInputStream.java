
package huffman.io;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.Math;

public class BitInputStream extends InputStream {
	private DataInputStream in;

	private byte buffer = 0;
	private int bufferindex = 0;
	private int[] mask = { 1, 2, 4, 8, 16, 32, 64, 128 };

	/**
	 * The primary constructor for BitInputStream.
	 * @param in DataInputStream is required
	 */
	public BitInputStream(DataInputStream in) {
		this.in = in;
	}

	/**
	 * This constructor is here for convenience.  It is a wrapper for BitInputStream(DataInputStream).
	 * @param inStream
	 */
	public BitInputStream(InputStream inStream) {
		this.in = new DataInputStream(in);
	}

	/**
	 * This constructor is here for convenience.  It is a wrapper for BitInputStream(DataInputStream).
	 * @param filename is the file that will be read
	 * @throws FileNotFoundException
	 */
	public BitInputStream(String filename) throws FileNotFoundException {
		this(new DataInputStream(new FileInputStream(filename)));
	}

	@Override
	public int available() throws IOException {
		return (int) (in.available() + Math.signum(bufferindex));
	}

	@Override
	public void close() throws IOException {
		in.close();
	}

	@Override
	public int read() throws IOException {
		return in.read();
	}

	/**
	 * Simple wrapper for the DataInputStream.
	 * @return next Integer in file
	 * @throws IOException
	 */
	public int readInt() throws IOException {
		return in.readInt();
	}

	/**
	 * Simple wrapper for the DataInputStream.
	 * @return next Byte in file
	 * @throws IOException
	 */
	public byte readByte() throws IOException {
		return in.readByte();
	}

	/**
	 * It is noteworthy that the bit is returned as an Integer not as a boolean.
	 * @return readBit() returns the bit value as an Integer 0 or 1
	 * @throws IOException
	 */
	public int readBit() throws IOException {
		if (bufferindex > 0) {
			bufferindex--;
		} else {
			if (in.available() > 0) {
				buffer = (byte) in.read();
				bufferindex = 7;
			} else {
				buffer = 0;
				bufferindex = 0;
			}
		}
		return (int) Math.signum(buffer & mask[bufferindex]);
	}

}
