package huffman.util;

import huffman.io.BitInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

public class Decompress extends CompressorUtil {
	BitInputStream INPUT_STREAM;
	OutputStream OUTPUT_STREAM;

	private int TREEWEIGHT = -1;

	public Decompress(BitInputStream inStream, OutputStream outStream) throws IOException {
		this.INPUT_STREAM = inStream;
		this.OUTPUT_STREAM = outStream;
	}

	public Decompress(InputStream inStream, OutputStream outStream) throws IOException {
		this(new BitInputStream(inStream), outStream);
	}

	public Decompress(BitInputStream inStream) throws IOException {
		this(inStream, System.out);
	}

	public Decompress(InputStream inStream) throws IOException {
		this(inStream, System.out);
	}

	public Decompress(String inFile, String outFile) throws IOException {
		this(new BitInputStream(inFile), new FileOutputStream(outFile));
	}

	public Decompress(String inFile) throws IOException {
		this(new BitInputStream(inFile));
	}
	
	@Override
	void loadTreeCustom() throws IOException {
		if (checkHeader()) {
			TREEWEIGHT = buildTree();
		}
	}

	@Override
	void writeDataCustom() throws IOException {
		if (TREEWEIGHT > 0) {
			readBytes(TREEWEIGHT);
		}
	}

	@Override
	void closeCustom() throws IOException {
		INPUT_STREAM.close();
		if (!simulate) {
			OUTPUT_STREAM.close();
		}
	}

	private boolean checkHeader() throws IOException {
		if(INPUT_STREAM == null) {
			System.out.println("This should not be null :(");
		}
		
		byte[] header = new byte[3];
		INPUT_STREAM.read(header);

		if (header[0] == 75 && header[1] == 67 && header[2] == 49) {
			System.out.println("Good header.");
			return true;
		} else {
			System.out.println("Bad header.");
			return false;
		}
	}

	private int buildTree() throws IOException {
		int weight = INPUT_STREAM.readInt();
		int size = INPUT_STREAM.readInt();

		List<Byte> blist = new Vector<Byte>();
		List<Integer> ilist = new Vector<Integer>();

		for (int i = 0; i < size; i++) {
			blist.add(INPUT_STREAM.readByte());
			ilist.add(INPUT_STREAM.readInt());
		}

		Node t = Node.load(blist, ilist);

		if (t.weight() == weight) {
			tree = t;
			return weight;
		} else {
			return -1;
		}
	}

	private void readBytes(int weight) throws IOException {
		List<Integer> vlist = new Vector<Integer>();
		int buffer = 0;
		while (INPUT_STREAM.available() > 0 && weight > 0) {
			buffer = INPUT_STREAM.readBit();
			vlist.add(buffer);
			try {
				OUTPUT_STREAM.write(tree.getChar(vlist));
				vlist.clear();
				weight--;
			} catch (Exception e) { // Catch exception if the any
				// TODO: add logging?
				// System.out.println("Could not find: "+vlist.toString());
			}
		}
	}
}
