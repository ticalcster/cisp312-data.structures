package huffman.util;

import huffman.io.BitOutputStream;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

public class CompressV2 extends CompressorUtil {
	BufferedInputStream INPUT_STREAM;
	BitOutputStream OUTPUT_STREAM;

	private int READLIMIT = 1073741824; // TODO: is this the best way? limit of
										// reading 1GB

	public CompressV2(BufferedInputStream inStream, BitOutputStream outStream) {
		this.INPUT_STREAM = inStream;
		this.OUTPUT_STREAM = outStream;
		this.INPUT_STREAM.mark(READLIMIT); // to reset the file, we need to read
											// it twice.
	}

	public CompressV2(InputStream inStream, OutputStream outStream) {
		this(new BufferedInputStream(inStream), new BitOutputStream(outStream));
	}

	public CompressV2(InputStream inStream) {
		this(inStream, System.out);
	}

	public CompressV2(String inFile, String outFile)
			throws FileNotFoundException {
		this(new FileInputStream(inFile), new FileOutputStream(outFile));
	}

	public CompressV2(String inFile) throws FileNotFoundException {
		this(new FileInputStream(inFile));
	}

	@Override
	void loadTreeCustom() throws IOException {
		/*
		 * count chars
		 */
		List<Node> chfq = countBytes();

		/*
		 * Sort / Add to node list.
		 */
		tree = sortBytes(chfq);

		/*
		 * create tree
		 */
		tree = Node.buildATree(tree); // TODO: remove the return
	}

	@Override
	void writeDataCustom() throws IOException {
		/*
		 * output binary data to stream
		 */
		headToStream();
		fileToStream();
	}

	@Override
	void closeCustom() throws IOException {
		INPUT_STREAM.close();
		if (!simulate) {
			OUTPUT_STREAM.close();
		}
	}

	private List<Node> countBytes() throws IOException {
		List<Node> chfq = new Vector<Node>();

		// initialize the array
		for (int i = 0; i <= 255; i++) {
			chfq.add(new Node((byte) i));
		}

		byte[] buffer = new byte[1];
		while ((INPUT_STREAM.read(buffer)) != -1) {
			chfq.get(unsignedToBytes(buffer[0])).inc();
		}

		return chfq;
	}

	private Node sortBytes(List<Node> chfq) {
		Node tree = null;
		boolean firstNode = true;

		for (int i = 0; i < chfq.size(); i++) {
			if (chfq.get(i).weight() > 0) {
				// TODO: remove second if
				if (firstNode) {
					tree = chfq.get(i);
					firstNode = false;
				} else {
					tree = tree.addSorted(chfq.get(i));
				}
			}
		}

		return tree;
	}

	private void headToStream() throws IOException {
		OUTPUT_STREAM.writeByte((byte) 75);
		OUTPUT_STREAM.writeByte((byte) 67);
		OUTPUT_STREAM.writeByte((byte) 49);

		List<Byte> blist = new Vector<Byte>();
		List<Integer> ilist = new Vector<Integer>();
		tree.save(blist, ilist);

		OUTPUT_STREAM.writeInt(tree.weight());
		OUTPUT_STREAM.writeInt(blist.size());

		for (int i = 0; i < blist.size(); i++) {
			OUTPUT_STREAM.writeByte(blist.get(i));
			OUTPUT_STREAM.writeInt(ilist.get(i));
		}

	}

	private void fileToStream() throws IOException {
		INPUT_STREAM.reset();

		byte[] buffer = new byte[1];
		while ((INPUT_STREAM.read(buffer)) != -1) {
			OUTPUT_STREAM.writeBit(tree.getBitArray(buffer[0]));
		}
	}

	private int unsignedToBytes(byte b) {
		return b & 0xFF;
	}
}
