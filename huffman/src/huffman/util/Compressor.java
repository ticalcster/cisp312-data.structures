/**
 * Compressor is the interface used for compressing and decompressing using Huffman encoding.
 */
package huffman.util;

import java.io.IOException;

public interface Compressor {
	void setSimulate(boolean sim);

	void run() throws IOException;

	void loadTree() throws IOException;

	void writeData() throws IOException;
	
	void close() throws IOException;

	void printTree();
	void printCompressionRatio();
	void printTimes();
}
