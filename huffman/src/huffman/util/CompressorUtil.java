/**
 * CompressorUtil is the abstract class that works with the Compressor interface.
 */
package huffman.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

abstract class CompressorUtil implements Compressor {
	protected InputStream INPUT_STREAM;
	protected OutputStream OUTPUT_STREAM;
	protected boolean simulate = false;

	private long TREE_TIME = 0;
	private long WRITE_TIME = 0;

	protected Node tree = null;

	public CompressorUtil() {
	}

	public CompressorUtil(InputStream inStream, OutputStream outStream) {
		// This is different with compress or decompress
	}

	public CompressorUtil(InputStream inStream) {
		this(inStream, System.out);
	}

	public CompressorUtil(String inFile, String outFile)
			throws FileNotFoundException {
		this(new FileInputStream(inFile), new FileOutputStream(outFile));
	}

	public CompressorUtil(String inFile) throws FileNotFoundException {
		this(new FileInputStream(inFile));
	}

	public final void setSimulate(boolean sim) {
		simulate = sim;
	}

	public final void run() throws IOException {
		loadTree();
		writeData();
		close();
	};

	public final void loadTree() throws IOException {
		long start = System.currentTimeMillis();
		loadTreeCustom();
		TREE_TIME = System.currentTimeMillis() - start;
	}

	public final void writeData() throws IOException {
		long start = System.currentTimeMillis();
		writeDataCustom();
		WRITE_TIME = System.currentTimeMillis() - start;
	}

	public final void close() throws IOException {
		closeCustom();
	}

	abstract void loadTreeCustom() throws IOException;

	abstract void writeDataCustom() throws IOException;

	abstract void closeCustom() throws IOException;

	public final void printTree() {
		tree.print();
	}

	public final void printCompressionRatio() {
		//TODO: this should not go directly to System.out
		System.out.format("Bits per Byte: %.2f%n", tree.optimal());
	}

	public final void printTimes() {
		//TODO: this should not go directly to System.out
		System.out.format("Tree built in %d ms%n", TREE_TIME);
		System.out.format("Data wrote in %d ms%n", WRITE_TIME);
	}
}