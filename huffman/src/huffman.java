import huffman.util.Compress;
import huffman.util.Compressor;
import huffman.util.Decompress;

import java.io.IOException;

import org.apache.commons.cli.AlreadySelectedException;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.MissingArgumentException;
import org.apache.commons.cli.MissingOptionException;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Parser;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.cli.UnrecognizedOptionException;

public class huffman {
	private final static String HUFFMAN_VERSION = "Version 1.7";

	@SuppressWarnings("static-access")
	public static void main(String[] args) throws IOException {
		Options opts = new Options();
		OptionGroup optgrp = new OptionGroup();

		opts.addOption(OptionBuilder.withLongOpt("help")
				.withDescription("print his help").create('h'));
		opts.addOption(OptionBuilder.withLongOpt("version")
				.withDescription("prints the application version").create('V'));
		opts.addOption(OptionBuilder.withLongOpt("verbose")
				.withDescription("print helpfull debugging info").create('v'));
		opts.addOption(OptionBuilder.withLongOpt("simulate")
				.withDescription("do everything except for write to file")
				.create('s'));
		
		// option group because you can't compress and decompress at the same time
		optgrp.addOption(OptionBuilder.withLongOpt("compress")
				.withDescription("compress a file").hasOptionalArgs(2).create('c'));
		optgrp.addOption(OptionBuilder.withLongOpt("decompress")
				.withDescription("decompress a file").hasArgs(2).create('d'));
		opts.addOptionGroup(optgrp);

		HelpFormatter formatter = new HelpFormatter();
		String usage = "huffman [OPTIONS] [FILES]";

		try {
			Parser parser = new PosixParser();
			CommandLine _cl = parser.parse(opts, args);

			if (_cl.hasOption('h')) {
				formatter.printHelp(usage, opts);
			} else if (_cl.hasOption('V')) {
				System.out.println(HUFFMAN_VERSION);
			} else {

			boolean verbose = (_cl.hasOption('v') ? true : false);
			boolean simulate = (_cl.hasOption('s') ? true : false);
				Compressor compv2 = null;
				String[] optargs = null;

				if(_cl.hasOption('c')) {
					System.out.println("Compressing...");
					optargs = _cl.getOptionValues('c');

					if (optargs.length == 1) {
						if (simulate) {
							compv2 = new Compress(optargs[0]);
						} else {
							compv2 = new Compress(optargs[0], optargs[0]
									+ ".huf");
						}
					} else if (optargs.length == 2) {
						if (simulate) {
							compv2 = new Compress(optargs[0]);
						} else {
							compv2 = new Compress(optargs[0], optargs[1]);
						}
					}
				} else if (_cl.hasOption('d')) {
					System.out.println("Decompressing...");
					optargs = _cl.getOptionValues('d');
					
					for(int i=0;i<optargs.length;i++){
						System.out.println(optargs[i]);
					}

					if (optargs.length == 1) {

					} else if (optargs.length == 2) {
						if (simulate) {
							compv2 = new Decompress(optargs[0]);
						} else {
							compv2 = new Decompress(optargs[0], optargs[1]);
						}
					}
				} else {
					formatter.printHelp(usage, opts);
				}
				if (compv2 != null) {
					if (simulate) {
						compv2.setSimulate(true);
						compv2.loadTree();
						compv2.close();
					} else {
						compv2.run();
					}

					if (verbose) {
						compv2.printTree();
						compv2.printCompressionRatio();
						compv2.printTimes();
					}
				}

			}

			// TODO: they all do the same thing, maybe consolidate?
		} catch (AlreadySelectedException e) {
			System.out.println(e.getMessage());
			formatter.printHelp(usage, opts);
		} catch (MissingArgumentException e) {
			System.out.println(e.getMessage());
			formatter.printHelp(usage, opts);
		} catch (MissingOptionException e) {
			System.out.println(e.getMessage());
			formatter.printHelp(usage, opts);
		} catch (UnrecognizedOptionException e) {
			System.out.println(e.getMessage());
			formatter.printHelp(usage, opts);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println( e.getMessage() );
		}
	}
}