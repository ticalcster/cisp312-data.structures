import java.io.*;
import java.util.*;
import javax.script.*;


public class ti108 {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Stack undo = new Stack(255);
		Object value = 0.0;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");
		String command = "";
				
		boolean exit = false;
		while(!exit) {
			try {
				command = br.readLine();
				
				//remove white space
				command = command.replaceAll("\\s","");
				
				if( command.equals("exit") ) {
					exit = true;
					System.out.println("bye bye");
				} else if( command.equals("help") ) {
					System.out.println(" Command      Action ");
					System.out.println("  exit         exits the calculator");
					System.out.println("  help         prints this help");
					System.out.println("  undo         undo the last operation");
					System.out.println("");
					System.out.println("Operators:");
					System.out.println("  +  add");
					System.out.println("  -  subtract");
					System.out.println("  *  multiply");
					System.out.println("  /  divide");
					System.out.println("  =  set current value");
					System.out.println("    (or just enter a number)");
					System.out.println("");
					System.out.println("Variables:");
					System.out.println("  ans         the last answer");
					System.out.println("");
					System.out.println("Examples:");
					System.out.println("  2+2         returns 4");
					System.out.println("  +4          returns [last answer]+4");
					System.out.println("  =34         returns 34");
					System.out.println("  34          returns 34");
					System.out.println("  3+ans/4     returns 3+[last answer]/4");
				} else if( command.equals("undo") ) {
					if( undo.isEmpty() ) {
						System.out.println("Nothing in undo stack, sorry.");
					} else {
						value = ((Memento<Object>) undo.pop()).getState();
						System.out.printf("%12f%n", Double.parseDouble(value.toString()) );
					}
				} else if(!command.equals("")) {
					if( undo.isFull() ) {
						System.out.println("Undo stack is full, sorry.");
					} else {
						Memento<Object> meme = new Memento<Object>();
						meme.setState(value);
						undo.push(meme);
					}
										
					value = engine.eval(buildExpression(value, command) );
					
					System.out.printf("%12f%n", Double.parseDouble(value.toString()) );
				} else {
					System.out.println("");
				}
				
								
			} catch (IOException e) {
				System.out.println("Try again, IO Error.");
			} catch (ScriptException e) {
				System.out.println("Try again, Script Error.");
			}
		}
		
	}
	
	private static String buildExpression(Object value, String command) {
		char[] oper = {'+','-','*','/'};
		Arrays.sort(oper);
		
		command = command.replaceAll("ans", value.toString() );
		
		if( Arrays.binarySearch(oper, command.charAt(0)) >= 0 ) {
			return value.toString() + command;
		} else if(command.charAt(0) == '=' && command.length() > 1) {
			return command.substring(1);
		}else {
			return command;
		}
	}
}
