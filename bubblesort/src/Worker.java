

public class Worker extends Thread {
	
	private boolean done = false;
	private int start = 0;
	private int end = 0;
	private int[] data;
	
	Worker(String threadName) {
		super(threadName); // Initialize thread.
		
		//System.out.println(this);
		//start();
	}
	
	public void run() {
		//Display info about this particular thread
		//System.out.println(Thread.currentThread().getName());
		boolean sorted = false;
		
		while(!sorted) {
			sorted = true;
			for(int i = this.start; i < this.end; i++) {
				if(this.data[i] > this.data[i+1]) {
					int temp = this.data[i+1];
					this.data[i+1] = this.data[i];
					this.data[i] = temp;
					sorted = false;
				}
			}
		}
		this.done = true;
	}
	
	public void arrayIn(int[] array, int start, int end) {
		this.done = false;
		this.data = array;
		this.start = start;
		this.end = end;
	}
/*	
	public int[] arrayOut() {
		return this.data;
	}
*/	
	public boolean Done() {
		return this.done;
	}
}