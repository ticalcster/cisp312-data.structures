import java.util.Random;

public class bsort {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//for timing
		long start = 0;
		long total = 0;
		
		//app vars
		int THREADS = 1;
		boolean done = false;
		Worker[] worker;
		int array_size = 500000;
		int[] array;
		
		/*
		 * Setup the environment 
		 */
		
		// CPU Count
		THREADS = getCPUCount();
		System.out.println("Using "+ THREADS +" threads.");
		
		// create array of random numbers
		array = new int[array_size];
		array = getArray(array_size);
		System.out.println("Random array created with size "+ array.length +".");
		
		// records the start time
		start = System.currentTimeMillis();
		
		/*
		 * THREADS is / by 2 in each loop.
		 * So if you have 8 cores the array will be splits into 8 sections. Each section
		 * will be sorted by it's own thread.  After the loop the number of threads is
		 * half, 4 threads.  Now each thread joins to already sorted sections.  This continues
		 * until there is only 1 thread left that joins the last two sorted sections.
		 */
		while(THREADS > 0) {
			
			// Thread setup
			worker = new Worker[THREADS];
			
			//create the threads threads
			for(int i = 0; i < THREADS;i++) {
				worker[i] = new Worker("Worker"+i);
			}
			
			//splits array into chunks to give to threads
			double preThread = (double)array_size / (double)THREADS;
			
			for(int i = 0; i < THREADS; i++) {
				int s = (int) Math.floor(preThread * i); // start index
				int t = (int) Math.floor(preThread * (i+1)) - 1; // end index - 1
							
				//System.out.println("thread:"+ i +" {start:"+ s +",end:"+ t +"}");
				
				//tells the worker what section to work on
				worker[i].arrayIn( array, s, t );
			}
			
			// start all the workers.
			for(int i = 0; i < THREADS;i++) {
				worker[i].start();
			}		
			
			/*
			 * this while waits until all the workers are done
			 */
			done = false;
			while (!done) {
				
				try {
					//wait a bit before we check again
					Thread.sleep(10);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				//check if all threads are done
				done = true;
				for(int i = 0; i < THREADS;i++) {
					if(!worker[0].Done() ) {
						done = false;
					}
				}
				
			}
			// all done
			
			// reduce the number of threads
			THREADS = (int)Math.floor( THREADS / 2 );
			
		}
		
		// calculate the time taken to sort the array
		total = System.currentTimeMillis() - start;
		System.out.format("Duration = %d ms", total );
		
	}
	
	private static int[] getArray(int s) {
		int[] a = new int[s];
		Random rand = new Random(); // initialize the random number generator. 
		
		for(int i = 0; i < s; i++) {
			a[i] = rand.nextInt(100) + 1; // n is between 1 and 100
		}
		
		return a;
	}
	private static int getCPUCount() {
		return Runtime.getRuntime().availableProcessors();
	}
}
