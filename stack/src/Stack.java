
public class Stack {
	private static int DEFAULT_SIZE = 25;
	private int current = -1;
	private Object[] stack;
	
	Stack() {
		this.stack = new Object[DEFAULT_SIZE];
	}
	Stack(int size) {
		this.stack = new Object[size];
	}
	
	public int size() {
		return this.current + 1;
	}
	public Object peek() {
		if( !this.isEmpty() ) {
			return this.stack[this.current];
		} else {
			return false;
		}
	}
	public boolean isEmpty() {
		if( this.current == -1) {
			return true;
		} else {
			return false;
		}
	}
	public boolean isFull() {
		if( this.stack.length - 1 == current ) {
			return true;
		} else {
			return false;
		}
	}
	public Object pop() {
		if( !this.isEmpty() ) {
			Object obj = this.stack[this.current];
			this.current--;
			return obj;
		} else {
			return false;
		}
	}
	public boolean push(Object obj) {
		if( !this.isFull() ) {
			this.current++;
			this.stack[this.current] = obj;
			return true;
		} else {
			return false;
		}
	}
}
